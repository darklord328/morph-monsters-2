ACTOR ImmuneMarine : Playerpawn
{
  Speed 1
  Health 100
  Radius 16
  Height 56
  Mass 100
  PainChance 255
  +NORADIUSDMG
  DamageFactor "Nukering", 0.0
  DamageFactor "AvaBall1", 0.0
  DamageFactor "AvaBall2", 0.0
  DamageFactor "AvaBall3", 0.0
  DamageFactor "AvaBall4", 0.0
  DamageFactor "hellshot2",0.0
  DamageFactor "BruiserFireSpawner",0.0
  DamageFactor "BruiserFire",0.0
  DamageFactor "ChaosRocket",0.0
  DamageFactor "Nukey",0.0
  DamageFactor "MortarSpawner",0.0
  DamageFactor "NukeBoomy",0.0
  DamageFactor "ChaosCharge",0.0
  DamageFactor "Nukey",0.0
  DamageFactor "NukeRing",0.0
  DamageFactor "HexMaker",0.0
  Damagefactor "HellFire", 0.0
  Damagefactor "HellGrenade",0.0
  Damagefactor "PyroBlast",0.0
  Damagefactor "Bfg10kshot",0.0
  Damagefactor "Bfg10k",0.0
  Player.DisplayName "ImmuneMarine2"
  Player.CrouchSprite "PLYC"
  Player.StartItem "Pistol"
  Player.StartItem "Fist"
  Player.StartItem "Clip", 50  
  //Player.StartItem "Bosseyegun"
  Player.ColorRange 112, 127 
  States
  {
  Spawn:
    PLAY A -1
    Loop
  See:
    PLAY ABCD 4 
    Loop
  Missile:
    PLAY E 12 
    Goto Spawn
  Melee:
    PLAY F 6 BRIGHT
    Goto Missile
  Pain:
    PLAY G 4 
    PLAY G 4 A_Pain
    Goto Spawn
  Death:
    PLAY H 0 A_PlayerSkinCheck("AltSkinDeath")
  Death1:
    PLAY H 5
    PLAY I 5 A_PlayerScream
    PLAY J 5 A_NoBlocking
    PLAY KLM 5
	PLAY N -1
    Stop
  XDeath:
    PLAY O 0 A_PlayerSkinCheck("AltSkinXDeath")
  XDeath1:
    PLAY O 5
    PLAY P 5 A_XScream
    PLAY Q 5 A_NoBlocking
    PLAY RSTUV 5
    PLAY W -1
    Stop
  AltSkinDeath:
    PLAY H 6
    PLAY I 6 A_PlayerScream
    PLAY JK 6
    PLAY L 6 A_NoBlocking
    PLAY MNO 6
    PLAY P -1
    Stop
  AltSkinXDeath:
    PLAY Q 5 A_PlayerScream
    PLAY R 0 A_NoBlocking
    PLAY R 5 A_SkullPop
    PLAY STUVWX 5
    PLAY Y -1
    Stop
  }
}


















ACTOR Tornadomarine : PlayerPawn
{
  Speed 1
  Health 100
  Radius 16
  Height 56
  Mass 100
  PainChance 255
  +NORADIUSDMG
  -FRIENDLY
  DamageFactor "Nukering", 0.0
  DamageFactor "AvaBall1", 0.0
  DamageFactor "AvaBall2", 0.0
  DamageFactor "AvaBall3", 0.0
  DamageFactor "AvaBall4", 0.0
  DamageFactor "hellshot2",0.0
  DamageFactor "BruiserFireSpawner",0.0
  DamageFactor "BruiserFire",0.0
  DamageFactor "ChaosRocket",0.0
  DamageFactor "Nukey",0.0
  DamageFactor "MortarSpawner",0.0
  DamageFactor "NukeBoomy",0.0
  DamageFactor "ChaosCharge",0.0
  DamageFactor "Nukey",0.0
  DamageFactor "NukeRing",0.0
  DamageFactor "HexMaker",0.0
  Damagefactor "HellFire", 0.0
  Damagefactor "HellGrenade",0.0
  Damagefactor "PyroBlast",0.0
  Damagefactor "Bfg10kshot",0.0
  Damagefactor "Bfg10k",0.0
  Player.DisplayName "ImmuneMarine3"
  Player.CrouchSprite "PLYC"
  Player.StartItem "Pistol"
  Player.StartItem "Fist"
  Player.StartItem "Clip", 50  
  //Player.StartItem "Bosseyegun"
  Player.ColorRange 112, 127

  States
  {
  Spawn:
    PLAY A -1
    Loop
  See:
    PLAY ABCD 4 
    Loop
  Missile:
    PLAY E 12 
    Goto Spawn
  Melee:
    PLAY F 6 BRIGHT
    Goto Missile
  Pain:
    PLAY G 4 
    PLAY G 4 A_Pain
    Goto Spawn
  Death:
    PLAY H 0 A_PlayerSkinCheck("AltSkinDeath")
  Death1:
    PLAY H 10
    PLAY I 10 A_PlayerScream
    PLAY J 10 A_NoBlocking
    PLAY KLM 10
    PLAY N -1
    Stop
  XDeath:
    PLAY O 0 A_PlayerSkinCheck("AltSkinXDeath")
  XDeath1:
    PLAY O 5
    PLAY P 5 A_XScream
    PLAY Q 5 A_NoBlocking
    PLAY RSTUV 5
    PLAY W -1
    Stop
  AltSkinDeath:
    PLAY H 6
    PLAY I 6 A_PlayerScream
    PLAY JK 6
    PLAY L 6 A_NoBlocking
    PLAY MNO 6
    PLAY P -1
    Stop
  AltSkinXDeath:
    PLAY Q 5 A_PlayerScream
    PLAY R 0 A_NoBlocking
    PLAY R 5 A_SkullPop
    PLAY STUVWX 5
    PLAY Y -1
    Stop
  }
}